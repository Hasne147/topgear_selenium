package selenium_topgear;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


import junit.framework.Assert;

public class Assignment_2 {
	
	static WebDriver driver1;
	
	
	public void set_up_browser2()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	void select_country()
	{
		int ch=1;
		switch(ch)
		{
		case 1 : driver1.get("https://www.olay.co.uk/en-gb");
		
		case 2 : driver1.get("https://www.olay.com.cn/");
		
		case 3 : driver1.get("https://www.olaz.de/de-de");
		
		case 4 : driver1.get(" https://www.olay.es/es-es");
		}
		
	}
	
	
	void registration_and_login() throws InterruptedException
	{
		
		
		
		//registration
		driver1.findElement(By.linkText("Register"));
		
		driver1.findElement(By.name("phdesktopbody_0$phdesktopbody_0_grs_account[emails][0][address]")).sendKeys("zxcvi@gmail.com");
		driver1.findElement(By.id("phdesktopbody_0_grs_account[password][password]")).sendKeys("olay123456");
		driver1.findElement(By.id("phdesktopbody_0_grs_account[password][confirm]")).sendKeys("olay123456");
		
		Select s1=new Select(driver1.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][day]")));
		s1.selectByVisibleText("12");
		
		Select s2=new Select(driver1.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][month]")));
		s2.selectByVisibleText("7");
		
		Select s3=new Select(driver1.findElement(By.id("phdesktopbody_0_grs_consumer[birthdate][year]")));
		s2.selectByVisibleText("1998");
		
		driver1.findElement(By.name("phdesktopbody_0$phdesktopbody_0_submit")).click();
		
		//verification
		String actual=driver1.findElement(By.cssSelector("span[class='welcomeMsgText']")).getText();
		String expected="Welcome";
		Assert.assertEquals(expected, actual);
		
		//log in 
		driver1.findElement(By.id("phdesktopheader_0_phdesktopheadertop_2_LogOffLink")).click();
		Thread.sleep(2000);
		driver1.findElement(By.id("driver1.findElement(By.id(\"phdesktopheader_0_phdesktopheadertop_2_LogOffLink\")).click();")).click();
		
		driver1.findElement(By.id("phdesktopbody_0_username")).sendKeys("zxcv@gmail.com");
		driver1.findElement(By.id("phdesktopbody_0_password")).sendKeys("olay123456");
		driver1.findElement(By.name("phdesktopbody_0$SIGN IN")).click();
		
		//verification
		String actual1=driver1.findElement(By.cssSelector("span[class='welcomeMsgText']")).getText();
		String expected1="Welcome";
		Assert.assertEquals(expected1, actual1);
		
		
	}
	
	
	


	public void close_browser2()
	{
		driver1.close();
		driver1.quit();
	}

	public static void main(String[] args) throws InterruptedException {
		
		Assignment_2 obj=new Assignment_2();
		obj.set_up_browser2();
		obj.select_country();
		obj.registration_and_login();
		obj.close_browser2();
		

	}

}
