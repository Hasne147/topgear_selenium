package selenium_topgear;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Assignment_1 {
	
	static WebDriver driver1;
	
	public void set_up_browser1()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	public void book_ticket() throws InterruptedException
	{
		//open website
		driver1.get("https://www.makemytrip.com/");
		
		//select one way trip
		driver1.findElement(By.xpath("//*[@id=\"SW\"]/div[1]/div[1]/ul/li[6]")).click();
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[1]/ul/li[1]")).click();
		
		//select from city
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[1]/label/span")).click();
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/input")).sendKeys("Mumbai");
		Thread.sleep(4000);
		driver1.findElement(By.id("react-autowhatever-1-section-0-item-0")).click();
		
		//select destination city
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[2]/label/span")).click();
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/div/div/div/input")).sendKeys("Delhi");
		Thread.sleep(3000);
		driver1.findElement(By.id("react-autowhatever-1-section-0-item-0")).click();
		
		//select date
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[3]/label")).click();
		Thread.sleep(3000);
		driver1.findElement(By.cssSelector("div[class='DayPicker-Day'][aria-label='Wed Jul 29 2020']")).click();
		
		//select passenger details
		driver1.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/div/div/div[2]/div[1]/div[5]/label")).click();
		String adults="adults-3";
		String children="children-3";
		String infants="infants-1";
		driver1.findElement(By.cssSelector("li[data-cy='"+adults+"']")).click();
		driver1.findElement(By.cssSelector("li[data-cy='"+children+"']")).click();
		driver1.findElement(By.cssSelector("li[data-cy='"+infants+"']")).click();
		Thread.sleep(3000);
		
		driver1.findElement(By.cssSelector("li[data-cy='travelClass-0']")).click();
		driver1.findElement(By.cssSelector("button[data-cy='travellerApplyBtn'][type='button']")).click();
		Thread.sleep(3000);
		
		//search flights
		driver1.findElement(By.cssSelector("p[data-cy='submit'][class='makeFlex vrtlCenter ']")).click();
		Thread.sleep(3000);
		
		//select lowest price
		driver1.findElement(By.xpath("//*[@id=\"bookbutton-RKEY:1d0db625-cba4-4369-94e7-6f255a919e7f:1_0\"]")).click();
		Thread.sleep(3000);
		
		//select book now
		driver1.findElement(By.xpath("//*[@id=\"fli_list_item_eebd59a1-359f-47ee-af33-934dccbe69ea\"]/div[3]/div[1]/div[2]/div[2]/div[2]/button")).click();
		Thread.sleep(3000);
		
		
	}
	
	public void close_browser()
	{
		driver1.close();
		driver1.quit();
	}

	public static void main(String[] args) throws InterruptedException {
		Assignment_1 obj=new Assignment_1();
	    obj.set_up_browser1();
	    obj.book_ticket();
	    obj.close_browser();

	}

}
