package selenium_topgear;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import junit.framework.Assert;

public class Assignment_3 {
	
	static WebDriver driver1;
	
	public void set_up_browser3()
	{
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium_softwares\\chromedriver_win32\\chromedriver.exe");
		driver1=new ChromeDriver();
		driver1.manage().window().maximize();
		driver1.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver1.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
	}
	
	public void selectable()
	{
		driver1.get(" https://demoqa.com/selectable/");
		driver1.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[1]")).click();
		driver1.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[2]")).click();
		driver1.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[3]")).click();
		driver1.findElement(By.xpath("//*[@id=\"verticalListContainer\"]/li[4]")).click();
		
		driver1.findElement(By.id("demo-tab-grid")).click();
		driver1.findElement(By.xpath("//*[@id=\"row2\"]/li[2]")).click();
		driver1.findElement(By.xpath("//*[@id=\"row2\"]/li[4]")).click();
		driver1.findElement(By.xpath("//*[@id=\"row2\"]/li[6]")).click();
	}
	
	public void droppable()
	{
		driver1.get("https://demoqa.com/droppable/");
		Actions a1 = new Actions(driver1);
		 
		 WebElement from = driver1.findElement(By.id("draggable"));
		 
		 WebElement to = driver1.findElement(By.id("droppable")); 
		 
		 a1.dragAndDrop(from, to).perform();
		 String actual = to.getText();
		 String expected="Dropped!";		 
		 Assert.assertEquals(expected, actual);
	}
	
	public void select_menu()
	{
		driver1.get("https://demoqa.com/select-menu");
		
		Select s1 =new Select(driver1.findElement(By.id("oldSelectMenu")));
		for(int i=0;i<10;i++)
		{
		s1.selectByIndex(i);
		}
		
		
	}
	
	public void date_picker()
	{
		driver1.get("https://demoqa.com/date-picker");
		
		driver1.findElement(By.id("datePickerMonthYearInput")).click();
		Select year=new Select(driver1.findElement(By.className("react-datepicker__year-select")));
		year.selectByValue("1998");
		
		Select month=new Select(driver1.findElement(By.className("react-datepicker__month-select")));
		month.selectByVisibleText("July");
		
		driver1.findElement(By.className("react-datepicker__day react-datepicker__day--014")).click();
	}
	
	public void close_browser3()
	{
		driver1.close();
		driver1.quit();
	}

	public static void main(String[] args) {
		Assignment_3 obj3=new Assignment_3();
		
		obj3.set_up_browser3();
		obj3.selectable();
		obj3.close_browser3();
		
		obj3.set_up_browser3();
		obj3.droppable();
		obj3.close_browser3();
		
		obj3.set_up_browser3();
		obj3.select_menu();
		obj3.close_browser3();
		
		obj3.set_up_browser3();
		obj3.date_picker();
		obj3.close_browser3();


	}

}
